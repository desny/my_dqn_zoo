# my_dqn_zoo
在deepmind团队的dqn_zoo代码基础上做了简单修改，使其可以实现测试阶段游戏训练效果的可视化。
原代码地址：https://github.com/deepmind/dqn_zoo

## Quick Start
#### 1. 安装前的准备
* 本代码只能在linux系统上运行；
* 有gpu，并已配置cuda和cudnn
* 确认jaxlib的版本

---
因为jaxlib库的版本与python、cuda和cudnn的版本号相关，所以需要知道 **python、cuda和cudnn的版本** ：
* python版本：在命令行直接输入python，得到版本信息(3.7)，如下图所示
![输入图片说明](tmp/readme_pics/2022-04-26_090127.png)

* cuda版本：在命令行输入nvidia-smi，得到版本信息(11.6)，如下图所示
![输入图片说明](tmp/readme_pics/2022-04-26_091608.png)

* cudnn版本：在命令行输入cat /usr/local/cuda/include/cudnn_version.h | grep CUDNN_MAJOR -A 2，得到版本信息(8.3.3)，如下图所示
![输入图片说明](tmp/readme_pics/2022-04-26_091914.png)

 **在已知python、cuda和cudnn版本后** ，在官网找到合适的jaxlib版本：
[官方jaxlib下载页面(nocuda+mac)](https://storage.googleapis.com/jax-releases/jax_releases.html)
[官方jaxlib下载页面(cuda)](https://storage.googleapis.com/jax-releases/jax_cuda_releases.html)
![输入图片说明](tmp/readme_pics/2022-04-26_092819.png)
将适合自己环境的jaxlib版本的下载链接在requirements.txt中进行替换：
![输入图片说明](tmp/readme_pics/2022-04-26_093217.png)

#### 2. 安装依赖库

 **确认好jaxlib版本后，开始正式安装依赖库** 
```
# 切换到my_dqn_zoo文件夹下
cd my_dqn_zoo

# 安装第三方库
pip install -r requirements.txt
```

#### 3. 修改dm-env
官方代码用dm-env库去封装gym环境，但是并没有实现render功能(即游戏界面渲染)。为了更直观地看到游戏的训练效果，需要找到dm-env库，修改类中的方法，使其支持游戏可视化：
```
# 在dm_env._environment->Environment中新增render
def render(self):
    pass

# 在gym_atari.py->GymAtari中继承并实现render
def render(self):
    self._gym_env.render()
```
![输入图片说明](tmp/readme_pics/2022-04-26_105332.png)
![输入图片说明](tmp/readme_pics/2022-04-26_105401.png)

#### 4. 运行注意事项
* 两种可视化游戏的方式

一共有两种方式可以可视化游戏，1）在训练结束后，直接使用最后得到的神经网络参数来操纵agent玩游戏。对应下图红框中被注释的代码；2）在训练过程中已经存储了一系列的参数文件，可以选一个参数文件，将其重命名为 *iteration.pkl* ，则可以直接加载权重文件来查看游戏效果。对应红框中未被注释的代码。
![输入图片说明](tmp/readme_pics/2022-04-26_151514.png)
想要以某种方式可视化，则保留其对应代码，并将另一种方法的代码注释掉。

tips. 如果要采用第2种方式可视化，意味着此时不需要训练过程。一个简单跳过训练过程，直接可视化的方法就是找到flag.cfg，修改num_iterations的值为1，即仅执行一轮训练。

* 命令行运行或是PyCharm运行

如果是在 **命令行** 执行代码，则需要通过命令行传参(参数定义在flag.cfg中)：
```
python dqn/run_atari.py --flagfile=flag.cfg
```
如果是在 **PyCharm** 中执行代码(方便debug)，则需要设定参数：
Run->edit configurations -> parameters
如果params中文件路径的话，建议写full path
以下图举例：
![输入图片说明](tmp/readme_pics/2022-04-20_164937.png)

#### 5. 运行代码
按照上述方式配置环境并运行代码即可复现基于DQN的系列算法！预祝大家研究愉快~